import java.util.Comparator;

public class Point implements Comparable<Point>{

    private final int x, y;

    //compare points by slope to this point
    public final Comparator<Point> SLOPE_ORDER = this.new SlopeOrdering();

    /**
     * Constructs a point(x,y)
     * @param x the x coordinate
     * @param y the y coordinate
     */
    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }
    /**
     * Draws a point using StdDraw on a coordinate system.
     */
    public void draw(){
        StdDraw.point(x, y);
    }
    /**
     * Draws a line segment from this point to another point using StdDraw.
     * @param that that other point
     */
    public void drawTo(Point that){
        StdDraw.line(this.x, this.y, that.x, that.y);
    }
    /**
     * A string representation of this point.
     * The format of the string is: (x y).
     *
     * @return a string representation of this point
     */
    public String toString(){
        return "(" + x + " " + y + ")";
    }
    /**
     * Is this point smaller than that point?.
     * Given points p and q, p is less than a point q if and only if
     * p.y less than q.y or if p.x less than p.y when p.y == q.y. Points p and
     * q are equal if and only if p.y == q.y and p.x == q.x. Otherwise p
     * greater than q.
     *
     * @param that that other point we compare to this point
     *
     * @return -1 if p is less than q; 0 if p is equal to q, 1 if p is greater
     * than q.
     */
    public int compareTo(Point that){
        if (this.y > that.y) return 1;
        else if (this.y == that.y){
            if (this.x > that.x) return 1;
            else if (this.x == that.x) return 0;
            else return -1;
        }
        else return -1;
    }
    /**
     * The slope between this point and that point.
     * If that other point has the same coordinates as this point, the slope is
     * NEGATIVE_INFINITY. If that other point makes a vertical slope with p, the slope is
     * POSITIVE_INFINITY. If that other point makes a horizontal slope with p,
     * the slope is 0. Otherwise, we calculate the slope.
     *
     * @param that that other point that makes a slope with p
     *
     * @return NEGATIVE_INFINITY if the coordinates of that is equal to the
     * coordinates of this, POSITIVE_INFINITY if the slope is vertical, 0.0d if
     * the slope is horizontal. Otherwise the slope is returned.
     */
    public double slopeTo(Point that){
        if (compareTo(that) == 0) return Double.NEGATIVE_INFINITY;
        else if (this.y == that.y) return 0.0d;
        else if (this.x == that.x) return Double.POSITIVE_INFINITY;
        else return slope(that);
    }
    /**
     * Calculates the slope between this point to that point.
     *
     * @param that that other point that makes a slope with this point
     *
     * @return the slope between this point and that point.
     */
    private double slope(Point that){
        return (double) (that.y - this.y) / (that.x - this.x);
    }
    /**
     * A comparator that will compare the slopes made between this point and two
     * other points.
     */
    private class SlopeOrdering implements Comparator<Point>{
        /**
         * Compares the slopes made between this point and two other points:
         * the slope between this and p against the slope between this and q.
         *
         * @return -1 if the slope with p is less than the slope with q, 0 if
         * the slopes are equal, 1 if the slopes with p is greater than the
         * slope with q.
         */
        public int compare(Point p, Point q){
            Point o = Point.this;
            return Double.compare(o.slopeTo(p), o.slopeTo(q));
        }
        /**
         * Compares this point against that other point.
         *
         * @return true if the coordinates of this point are equal to that
         * point. Otherwise false.
         */
        public boolean equals(Point that){
            return Point.this.compareTo(that) == 0;
        }
        
    }
    /**
     * Main function.
     * Contains unit tests for this class.
     *
     * @param args arguments from the command line
     */
    public static void main(String[] args){
        //Testing Constructor
        Point p = new Point(1,2);
        Point q = new Point(3,4);

        //Testing the toString() method
        System.out.println("p = " + p.toString() + ", q = " + q.toString());

        //Testing the draw method
        //Setting the scale of the coordinate system from 0 to 10 for both axis.
        StdDraw.setXscale(0, 10);
        StdDraw.setYscale(0, 10);
        p.draw();
        q.draw();

        //Testing the drawTo method
        p.drawTo(q);

        //Testing the compareTo method
        Point u = new Point(1,3);
        Point v = new Point(1,1);
        System.out.println("u = " + u.toString() + ", v = " + v.toString());
        System.out.println("p.compareTo(q) == " + p.compareTo(q));
        System.out.println("q.compareTo(p) == " + q.compareTo(p));
        System.out.println("p.compareTo(u) == " + p.compareTo(u));
        System.out.println("p.compareTo(v) == " + p.compareTo(v));
        System.out.println("p.compareTo(p) == " + p.compareTo(p));

        //Testing the slopeTo method
        Point r = new Point(2, 2);
        Point s = new Point(1, 1);
        System.out.println("r = " + p.toString() + ", s = " + q.toString());
        System.out.println("p.slopeTo(r) == " + p.slopeTo(r));
        System.out.println("p.slopeTo(s) == " + p.slopeTo(s));
        System.out.println("p.slopeTo(p) == " + p.slopeTo(p));
        System.out.println("p.slopeTo(q) == " + p.slopeTo(q));

        Point j = new Point(21000, 10000);
        Point k = new Point(1234, 5678);
        System.out.println("j = " + j.toString() + ", k = " + k.toString());
        System.out.println("j.slopeTo(k) == " + j.slopeTo(k));

        //Testing the comparator
        Comparator<Point> c = p.SLOPE_ORDER;
        Point t = new Point(10, 5);
        System.out.println("t = " + t.toString());
        System.out.println("Compare slopes of p,q & p,t: " + c.compare(q, t));
        System.out.println("Compare slopes of p,t & p,q: " + c.compare(t, q));
    }


}
