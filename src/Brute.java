/**
* A brute-force solution whose running time has an order of growth of N^4.
* Additionally, it uses space proportional to N.
*
* The {@code Brute} class makes use of the {@code Point} class to
* represent points from a 2D-coordinate system. In addition, it also makes
* use of the {@code In} class from {@code stdlib} to read input from
* files.
*/
public class Brute{
    /**
    * Starts the brute-force algorithm.
    *
    * @param args arguments from the command line
    */
    public static void main(String[] args){
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        
        //Setting the scale of the coordinate system
        StdDraw.setXscale(0, 32767);
        StdDraw.setYscale(0, 32767);
        
        for (int i = 0; i < n; i++){
            points[i] = new Point(in.readInt(), in.readInt());
            points[i].draw();
        }

        int p = 0;

        for (int i = 0; i < n; i++)
            for (int j = i+1; j < n; j++)
                for (int k = j+1; k < n; k++)
                    for (int l = k+1; l < n; l++)
                        if (isCollinear(points[i], points[j], points[k], points[l])){
                            printSegment(points[i], points[j], points[k], points[l]);
                            drawSegment(points[i], points[j], points[k], points[l]);
                        }

    }
    /**
     * Checks whether or not four points are collinear.
     *
     * @param a 1st point
     * @param b 2nd point
     * @param c 3rd point
     * @param d 4th point
     *
     * @return Returns {@code false} if the points are not collinear, otherwise
     *         {@code true}.
     */
    private static boolean isCollinear(Point a, Point b, Point c, Point d){
        return Double.compare(a.slopeTo(b), a.slopeTo(c)) == 0 &&
            Double.compare(a.slopeTo(c), a.slopeTo(d)) == 0;
    }
    /**
     * Prints a line segment as a string to the console.
     * A line segment consisting of 4 collinear points is printed in
     * following format:
     *
     * (x1,y1) -> (x2, y2) -> (x3, y3) -> (x4, y4)
     *
     * @param a 1st point
     * @param b 2nd point
     * @param c 3rd point
     * @param d 4th point
     */
    private static void printSegment(Point a, Point b, Point c, Point d){
        String s = String.format("%s -> %s -> %s -> %s", 
                a.toString(), b.toString(), c.toString(), d.toString());
        System.out.println(s);
    }
    /**
     * Draws a line segment to a GUI coordinate system.
     * If the points are collinear, then the line segment will connect all four
     * points.
     *
     * @param a a point
     * @param b a point
     * @param c a point
     * @param d a point
     */
    private static void drawSegment(Point a, Point b, Point c, Point d){
        Point[] points = {a, b, c, d};
        Point minEndpoint = minPoint(points);
        Point maxEndpoint = maxPoint(points);
        minEndpoint.drawTo(maxEndpoint);
    }
    /**
     * Find the smallest point in an array of points.
     *
     * @param points an array of points
     *
     * @return the smallest point in the array of points
     */
    private static Point minPoint(Point[] points){
        Point min = points[0];
        for (Point p : points)
            if (min.compareTo(p) > 0)
                min = p;
        return min;
    }
    /**
     * Finds the largest point in an array of points.
     *
     * @param points an array of points
     *
     * @return the smallest point in the array of points
     */
    private static Point maxPoint(Point[] points){
        Point max = points[0];
        for (Point p : points)
            if (max.compareTo(p) < 0)
                max = p;
        return max;
    }

}
