/**
 * A fast algorithm that finds all sets of collinear points of size greater
 * than or equal to 3.
 */
public class Fast{
    /**
     * Implements a fast algorithm of finding collinear points.
     * Finds all points collinear to each of the given N points, in sorted
     * order. The algorithm has a growth order proportional to N^2 lg N and uses
     * space proportional to N.
     */
    public static void main(String[] args){
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        Point[] copyPoints = new Point[n];
        
        for (int i = 0; i < n; i++)
            points[i] = new Point(in.readInt(), in.readInt());

        for (int i = 0; i < n; i++)
            copyPoints[i] = points[i];

        drawPoints(points);

        //Finding all points collinear to the smallest point participating in
        //that set implies that all points collinear to other points in the set
        //beside the smallest point are subsets. Discovering subsets requires
        //checking if at least one of the points collinear to p is less than p.
        Arrays.sort(points);
        
        for (int i = 0; i < n; i++)
            findCollinearPoints(points[i], copyPoints);
    }
    /**
     * Finds all points collinear to a given point p.
     * Taking p to be an origin, points that are collinear to p are found
     * according to the slope that is made with p. Adjacent points that make the
     * same slope with p are points collinear to p. If the number of these
     * adjacent collinear points is greater than or equal to 3, then they,
     * together with p, are printed to the console and drawn.
     *
     * @param p a given point
     * @param points a set of points
     */
    private static void findCollinearPoints(Point p, Point[] points){
        //This first sort is necessary to keep collinear points ordered.
        //It also undoes any previous sorts done on points.
        Arrays.sort(points);
        Arrays.sort(points, p.SLOPE_ORDER);

        double[] slopes = new double[points.length];

        for (int i = 0; i < points.length; i++)
            slopes[i] = p.slopeTo(points[i]);

        int i = 1;
        //Sections of contiguous points that have the same slopes with p in the
        //array. The first pointer i indicates the beginning of the section and
        //j iterates from i until the slopes are no longer equal, indicating the
        //end of the section.
        while (i < points.length-1){
            int j = i+1;
            while (j < points.length && Double.compare(slopes[i], slopes[j]) == 0)
                j++;
            if (j - i >= 3 && p.compareTo(points[i]) <= 0){
                printSegment(points, p , i, j);
                p.drawTo(points[j-1]);
            }
            i = j;
        }
    }
    /**
     * Given a subarray that is ordered according to natural order, the point p
     * along with all other points collinear to p are printed to console in
     * natural order.
     *
     * @param points an array of points
     * @param p a point p
     * @param lo the beginning of the subarray containing points collinear to p
     * @param hi the end of the subarray + 1
     */
    private static void printSegment(Point[] points, Point p, int lo, int hi){
        StringBuilder segment = new StringBuilder();

        segment.append(p);
        for (int i = lo; i < hi; i++)
            segment.append(" -> " + points[i]);

        System.out.println(segment.toString());
    }
    /**
     * Draws all points in a given array on the coordinate system.
     *
     * @param points an array of points     *
     */
    private static void drawPoints(Point[] points){
        StdDraw.setXscale(0, 32767);
        StdDraw.setYscale(0, 32767);
        for (Point p : points)
            p.draw();
    }
}
